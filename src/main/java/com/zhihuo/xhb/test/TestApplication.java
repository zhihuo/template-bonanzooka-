package com.zhihuo.xhb.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


/**
 * Created   on 2015/10/26.
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class TestApplication {

	@Bean
	public InternalResourceViewResolver internalViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/pages/");
		resolver.setSuffix(".html");
		return resolver;
	}


	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}

}